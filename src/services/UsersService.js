import Api from '@/services/Api'

export default {
    login(data) {
        return Api().post('/auth/login', data)
    },

    register(data) {
        return Api().post('/auth/register', data)
    },

    me(token) {
        return Api().post('/auth/user', {
            headers: {
                Authorization: "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiNzRlNjY2ZDM5ZGEyMzk2OGE3Y2Q0MzU3NDMwM2YxNzZkODVlOGRjYThlMzJjNzhlMmNlMWFhZjJiODcxYTUxZWEyODE0ZjdkZmM5YjFmZWIiLCJpYXQiOjE2NzQ0MjMzMDcuNTA0ODE3LCJuYmYiOjE2NzQ0MjMzMDcuNTA0ODIzLCJleHAiOjE3MDU5NTkzMDcuMzMyOTkyLCJzdWIiOiI3Iiwic2NvcGVzIjpbXX0.E7sAHgxHqNdw8WlDPTdMoroG_aEIrg-__Fjt3-khao6M96HtNLcoq_L3A720Ixm9oZOhbUbmTiR0Ym4amhxdN3jYGEbNK115cJjekdXrww7PG5CNcNMurrZwjZElbIKJO6YPjFRyt8XGZ67liOKjZfa_EHXfTIEAP26eDiGS4Xzr0wOPjaODDbvK5IRBaGM74j9xRcvIBtIMC3BcDCJao39k361nPzXsK64UuV3gt9e-hpQ3gXXQBG8_1o7s0ExU7YDA4HO7Ona-6NcE-vHm4FVTV0TIkz0Up9fsJyPH9qENd-SsJRzaK3rNUU2qOKFFmkk-eElIsprrpVFGL5ydWK8gYAPlO1FiT5dqwEzBazGg7ADx1ODcmLuhynANV8Ct8gzMuE8cDtKjswb-aiz7_-P-z9bqU2kuG_yfGYOivMLHkpyJ8iJiCGEKkPUFBekNn_QfBWw4cxSUhjs9eZHDjXlcRc6CtpP697x-GhYKLIFQ_XWBjiaGDuRwzgSh1wB_qNfg7OlKeRG2x--F93PH6xTEV-OokhDlUtRrWpCcotgMzl26LhAFND5rxifWZ9Ic8NUo1f0uWuIDDxaSwT-3q4Bioq4M7qZeEhYhL7Oe-mQsd4n5DiwliMb3mMCzBuYA6yfCbG3aNl0KeDFE7qSS42e0g_MGmwMr4JA1Pblrwng"
            }
         })
     },

     getUsers() {
        return Api().get('/users/')
     },

     registerUser(data){
        return Api().post('/users/', data)
     },

     getUser(uuid) {
        return Api().get('/users/data/'+uuid)
     },

     updatePassword(data, uuid)
     {
        return Api().post('/users/updatePassword/'+uuid, data)
     },

     updateUser(data, uuid) {
        return Api().post('/users/update/'+uuid, data)
     }
}