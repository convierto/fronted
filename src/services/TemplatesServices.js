import Api from '@/services/Api'

export default {

    get() {
        return Api().get('/templates')
    },

    store(data) {
        return Api().post('/templates', data)
    },

    details(uuid) {
        return Api().get('/templates/details/'+uuid)
    },

    update(data, uuid) {
        return Api().post('/templates/update/'+uuid, data)
    }
}