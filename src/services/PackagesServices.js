import Api from '@/services/Api'

export default {
    getPackages () {
        return Api().get('/packages/')
    },

    storePackage(data) {
        return Api().post('/packages/', data)
    },

    desactivarPackage(uuid) {
        return Api().get('/packages/desactivar/'+uuid)
    },

    activarPackage(uuid) {
        return Api().get('/packages/activar/'+uuid)
    }
}