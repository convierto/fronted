import Api from '@/services/Api'

export default {

    byTemplate(uuid) {
        return Api().get('/messages/get/'+uuid)
    },

    store(data){
        return Api().post('/messages/', data)
    },

    delete(uuid){
        return Api().get('/messages/delete/'+uuid)
    },

    get(uuid) {
        return Api().get('/messages/details/'+uuid)
    },

    update(data, uuid){
        return Api().post('/messages/update/'+uuid, data)
    }

}