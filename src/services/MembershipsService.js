import Api from '@/services/Api'

export default {

    byUser(uuid) {
        return Api().get('/memberships/user/'+uuid)
    }
}