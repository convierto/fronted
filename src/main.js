import { createApp } from 'vue'
import App from './App.vue'
import router from './router'


import {LoadingPlugin} from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/css/index.css';

import { vfmPlugin } from 'vue-final-modal'

import '@/assets/css/bootstrap.min.css'
import '@/assets/css/owl.theme.default.min.css'
import '@/assets/css/animate.min.css'
import '@/assets/css/boxicons.min.css'
import '@/assets/css/metismenu.min.css'
import '@/assets/css/simplebar.min.css'
import '@/assets/css/calendar.css'
import '@/assets/css/sweetalert2.min.css'
import '@/assets/css/jbox.all.min.css'
import '@/assets/css/font-awesome.min.css'
import '@/assets/css/loaders.css'
import '@/assets/css/header.css'
import '@/assets/css/sidebar-menu.css'
import '@/assets/css/footer.css'
import '@/assets/css/style.css'
import '@/assets/css/responsive.css'
import '@/assets/css/newStyles.css'

createApp(App).use(router).use(vfmPlugin).use(LoadingPlugin).mount('#app')

