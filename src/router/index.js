import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/users',
    name: 'users',
    component: () => import('@/views/users/Index')
  },
  {
    path: '/user/create',
    name: 'create-user',
    component: () => import('@/views/users/Create')
  },
  {
    path: '/user/edit/:uuid',
    name: 'edit-user',
    component: () => import('@/views/users/Edit')
  },
  {
    path: '/user/membresia/:uuid',
    name: 'user-membresia',
    component: () => import('@/views/memberships/UsersDetails')
  },
  {
    path: '/user/pays/:uuid',
    name: 'user-pagos',
    component: () => import('@/views/pays/UserDetails')
  },
  {
    path: '/packages',
    name: 'packages',
    component: () => import('@/views/packages/Index')
  },
  {
    path: '/packages/create',
    name: 'packages-create',
    component: () => import('@/views/packages/Create')
  },
  {
    path: '/pays',
    name: 'pays',
    component: () => import('@/views/pays/Index')
  },
  {
    path: '/memberships',
    name: 'memberships',
    component: () => import('@/views/memberships/Index')
  },
  {
    path: '/templates',
    name: 'templates',
    component: () => import('@/views/templates/Index')
  },
  {
    path: '/template/create',
    name: 'template-create',
    component: () => import('@/views/templates/Create')
  },
  {
    path: '/template/edit/:uuid',
    name: 'edit-template',
    component: () => import('@/views/templates/Edit')
  },
  {
    path: '/messages/:uuid',
    name: 'messages',
    component: () => import('@/views/templates_messages/Index')
  },
  {
    path: '/message/create/:uuid',
    name: 'message-create',
    component: () => import('@/views/templates_messages/Create')
  },
  {
    path: '/message/edit/:uuid',
    name: 'messsage-edit',
    component: () => import('@/views/templates_messages/Edit')
  },
  {
    path: '/logout',
    name: 'logout',
    component: () => {
      localStorage.clear()
      location.reload()
    }
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
